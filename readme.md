# intro
some mini toolkits / user scripts for greasemonkey

# file list
1. dict.cn.user.js :
  dict.cn query for mouse selection word
2. douban_Photo_Title.user.js :
  douban photo album helper for change image display size to batch download large source
3. hjdict.user.js :
  hjdict query for mouse selection word
4. hotkey_next_page.user.js :
  left/right key to load prev/next page
5. music.163.com_cover.user.js :
  add fancybox to album/playlist image in album/playlist/song page
6. zdic.cn.user.js :
  zdic query for mouse selection word
7. zhihu.user.js :
  zhihu remove login notify for read more

# files in download page
1. WaterMarkEx.7z :
  batch add watermark to images, support some pre-processing/effect
2. WaterMarkEx_GP10.7z :
  batch add watermark to images, support some pre-processing/effect, but only using at GDIPlus 1.0(WinXP)
3. AutoBrightness.7z :
  automatic adjust notebook brightness with pre-calc curve, default script is sine wave.
4. MiniAlarm.7z :
  mini countdown alerm with quick item support
5. M3UMaker.7z :
  automatic make m3u file with selected folder
6. ColorPicker.7z :
  simple color picker with GIMP color palette support
7. GoogleMaps.7z :
  simple google map save to image tool
8. SendInfoToMSN.7z :
  send text message to MSN status box
